/**
 * 
 */
package com.testapp.advert;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.SimpleUrlLogoutSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@Component
public class CustomLogoutSuccessHandler extends SimpleUrlLogoutSuccessHandler {


	
	@Override
	public void onLogoutSuccess(HttpServletRequest request,
                                HttpServletResponse response, Authentication authentication) throws IOException,
            ServletException {
		

		if(null != authentication && null != authentication.getDetails()) {
			try {
				//logger.info("Session invalidated");
				request.getSession().invalidate();
			} catch (Exception e) {
			}
			setDefaultTargetUrl("/home");
			super.onLogoutSuccess(request, response, authentication);
		}
	}

}
