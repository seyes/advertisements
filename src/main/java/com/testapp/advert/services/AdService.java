package com.testapp.advert.services;

import com.testapp.advert.model.Ad;
import com.testapp.advert.model.User;

import java.util.List;

/**
 * Created by svnikitin on 20.12.2017.
 */

public interface AdService {
    List<Ad> findAll();
    List<Ad> findLatest5();
    Ad findById(Long id);
    Ad create(Ad ad,User user);
    Ad edit(Ad ad);
    void deleteById(Long id);

    void updateAd(Ad ad);
}