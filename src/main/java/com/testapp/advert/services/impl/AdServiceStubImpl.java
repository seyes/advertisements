package com.testapp.advert.services.impl;

import com.testapp.advert.dao.AdDao;
import com.testapp.advert.dao.UserDao;
import com.testapp.advert.model.Ad;
import com.testapp.advert.model.User;
import com.testapp.advert.services.AdService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Created by svnikitin on 20.12.2017.
 */
@Service
public class AdServiceStubImpl implements AdService {

    @Autowired
    private AdDao adDao;

    private List<Ad> ads = new ArrayList<Ad>() {{
        add(new Ad(1L, "First Post", "<p>Line #1.</p><p>Line #2</p>"));
//        add(new Ad(2L, "Second Post",
//                "Second post content:<ul><li>line 1</li><li>line 2</li></p>",
//                new User(10L, "pesho10", "Peter Ivanov")));
//        add(new Post(3L, "Post #3", "<p>The post number 3 nice</p>",
//                new User(10L, "merry", null)));
//        add(new Post(4L, "Forth Post", "<p>Not interesting post</p>", null));
//        add(new Post(5L, "Post Number 5", "<p>Just posting</p>", null));
//        add(new Post(6L, "Sixth Post", "<p>Another interesting post</p>", null));
    }};

    @Override
    public List<Ad> findAll() {
        return adDao.getAllAds();
    }

    @Override
    public List<Ad> findLatest5() {
        return this.ads.stream()
                .sorted((a, b) -> b.getId().compareTo(a.getId()))
                .limit(5)
                .collect(Collectors.toList());
    }

    @Override
    public Ad findById(Long id) {
        return this.ads.stream()
                .filter(p -> Objects.equals(p.getId(), id))
                .findFirst()
                .orElse(null);
    }

    @Override
    public Ad create(Ad ad, User user) {
        return  adDao.addAd(ad,user);
    }


    @Override
    public Ad edit(Ad post) {
        for (int i = 0; i < this.ads.size(); i++) {
            if (Objects.equals(this.ads.get(i).getId(), post.getId())) {
                this.ads.set(i, post);
                return post;
            }
        }
        throw new RuntimeException("Ad not found: " + post.getId());
    }

    @Override
    public void deleteById(Long id) {

    }

    @Override
    public void updateAd(Ad ad) {
        adDao.update(ad);
    }
}