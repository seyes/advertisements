package com.testapp.advert.services;

/**
 * Created by svnikitin on 20.12.2017.
 */
public interface NotificationService {
    void addInfoMessage(String msg);
    void addErrorMessage(String msg);
}
