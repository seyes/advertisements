package com.testapp.advert.services;

import com.testapp.advert.model.User;

/**
 * Created by snikitin on 23.12.17.
 */
public interface UserService {
    boolean authenticate(String username, String password);

    boolean registerUser(String username, String password, String name);

    boolean isAuthenticated();

    User currentUser();
}
