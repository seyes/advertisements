package com.testapp.advert.controller;


import com.testapp.advert.forms.AdForm;
import com.testapp.advert.model.Ad;
import com.testapp.advert.model.User;
import com.testapp.advert.services.AdService;
import com.testapp.advert.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Controller
public class HomeController {

    @Autowired
    private AdService postService;

    //@Autowired
    //private NotificationService notificationService;

    @RequestMapping("/")
    public String home(Model model) {
        List<Ad> all = postService.findAll();
        model.addAttribute("allAds", all);
        return "home";
    }

    @GetMapping("/home")
    public String homeDir(Model model) {
        List<Ad> all = postService.findAll();
        model.addAttribute("allAds", all);
        return "/home";
    }

}