package com.testapp.advert.controller;

import com.testapp.advert.forms.AdForm;
import com.testapp.advert.forms.RegisterForm;
import com.testapp.advert.model.Ad;
import com.testapp.advert.services.AdService;
import com.testapp.advert.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * Created by svnikitin on 27.12.2017.
 */
@Controller
public class PostController {

    @Autowired
    private UserService userService;

    @Autowired
    private AdService postService;

    @RequestMapping("/posts/view/{id}")
    public String view(@PathVariable("id") Long id,
                       Model model) {
        Ad post = postService.findById(id);

        if (post == null) {
//            notificationService.addErrorMessage(
//                    "Cannot find post: " + id);
            return "redirect:/";
        }

        model.addAttribute("post", post);
        return "/posts/index";
    }

    @RequestMapping(value = "/posts/index", method = RequestMethod.GET)
    public String showUserPosts(Model model) {
        List<Ad> all = postService.findAll();
        model.addAttribute("allAds", all);
        return "posts/index";
    }

    @RequestMapping(value = "/posts/create", method = RequestMethod.GET)
    public String showPostForm(AdForm adForm) {
        Ad ad = new Ad();
        ad = postService.create(ad,userService.currentUser());
        adForm.setId(ad.getId());
        return "posts/create";
    }

    @RequestMapping(value = "/posts/create", method = RequestMethod.POST)
    public String updatePost(AdForm adForm){
        Ad ad = new Ad();
        ad.setId(adForm.getId());
        ad.setTitle(adForm.getTitle());
        ad.setDecription(adForm.getDecription());
        ad.setUser(userService.currentUser());
        postService.updateAd(ad);
        return "redirect:/";
    }
}
