package com.testapp.advert.controller;

import com.testapp.advert.forms.AdForm;
import com.testapp.advert.forms.LoginForm;
import com.testapp.advert.forms.RegisterForm;
import com.testapp.advert.services.NotificationService;
import com.testapp.advert.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;

/**
 * Created by snikitin on 23.12.17.
 */
@Controller
public class LoginController {

    @Autowired
    private UserService userService;

    @Autowired
    private NotificationService notifyService;

    @RequestMapping("/users/login")
    public String login(LoginForm loginForm) {
        return "users/login";
    }

    @RequestMapping(value = "/users/login", method = RequestMethod.POST)
    public String loginPage(@Valid LoginForm loginForm, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            notifyService.addErrorMessage("Please fill the form correctly!");
            return "users/login";
        }

//        if (!userService.authenticate(
//
//                loginForm.getUsername(), loginForm.getPassword())) {
//            notifyService.addErrorMessage("Invalid login!");
//            return "users/login";
//        }

        notifyService.addInfoMessage("Login successful");
        return "redirect:/";
    }

    @RequestMapping(value = "/users/register", method = RequestMethod.GET)
    public String showPostForm(RegisterForm registerForm) {
        return "users/register";
    }

    @RequestMapping(value = "/users/register", method = RequestMethod.POST)
    public String registerPage(@Valid RegisterForm registerForm, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            notifyService.addErrorMessage("Please fill the form correctly!");
            return "users/register";
        }

//        if (!userService.authenticate(
//
//                registerForm.getUsername(), registerForm.getPassword())) {
//            notifyService.addErrorMessage("Invalid register!");
//            return "users/register";
//        }

        if(!userService.registerUser(registerForm.getUsername(),registerForm.getPassword(),registerForm.getName())){
            notifyService.addErrorMessage("Invalid register!");
            return "users/register";
        }

        notifyService.addInfoMessage("Register successful");
        return "redirect:/";
    }
}
