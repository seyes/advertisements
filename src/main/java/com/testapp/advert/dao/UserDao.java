package com.testapp.advert.dao;

import com.testapp.advert.model.User;

/**
 * Created by svnikitin on 25.12.2017.
 */
public interface UserDao {

    boolean userExists(String login, String password);

    void addUser(User user);

    User findByUsernameIgnoreCase(String name);
}
