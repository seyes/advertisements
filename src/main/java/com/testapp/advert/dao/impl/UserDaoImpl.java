package com.testapp.advert.dao.impl;

import com.testapp.advert.dao.UserDao;
import com.testapp.advert.model.User;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 * Created by svnikitin on 25.12.2017.
 */
@Repository
public class UserDaoImpl implements UserDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public boolean userExists(String login, String password) {
        return false;
    }

    @Override
    @Transactional
    public void addUser(User user) {
        entityManager.persist(user);
        return;
    }

    @Override
    public User findByUsernameIgnoreCase(String nmae) {
        TypedQuery<User> tq = entityManager.createQuery("from User WHERE username=?", User.class);
        User result = tq.setParameter(1, nmae).getSingleResult();
        return result;
    }
}
