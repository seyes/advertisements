package com.testapp.advert.dao.impl;

import com.testapp.advert.dao.AdDao;
import com.testapp.advert.model.Ad;
import com.testapp.advert.model.User;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created by svnikitin on 27.12.2017.
 */
@Repository
public class AdDaoImpl implements AdDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    @Transactional
    public Ad addAd(Ad ad, User user) {
        ad.setUser(user);
        entityManager.merge(ad);
        return ad;
    }

    @Override
    public List<Ad> getAllAds(){
        return  entityManager.createQuery("Select a From Ad a",
                Ad.class).getResultList();
    }

    @Override
    @Transactional
    public void update(Ad ad) {
        //entityManager.find(Ad.class,ad.getId());
        entityManager.merge(ad);
    }
}
