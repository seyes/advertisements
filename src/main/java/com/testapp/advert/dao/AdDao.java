package com.testapp.advert.dao;

import com.testapp.advert.model.Ad;
import com.testapp.advert.model.User;

import java.util.List;

/**
 * Created by svnikitin on 27.12.2017.
 */
public interface AdDao {

    Ad addAd(Ad ad, User user);

    List<Ad> getAllAds();

    void update(Ad ad);
}
